import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:async_helper_mixin/async_helper_mixin.dart';

class _StreamTestWidget extends StatelessWidget with StreamBuilderHelperMixin {
  final Stream<int> stream;

  _StreamTestWidget({@required this.stream});

  @override
  Stream<Widget> buildStream(BuildContext context) =>
      stream.map((result) => Text('Stream result is $result'));
}

class _StreamTestWidgetWithCustomDefaultWidget extends StatelessWidget
    with StreamBuilderHelperMixin {
  final Stream<int> stream;

  _StreamTestWidgetWithCustomDefaultWidget({@required this.stream});

  @override
  Widget get defaultWidget => Text('Stream doesn\'t completed yet');

  @override
  Stream<Widget> buildStream(BuildContext context) =>
      stream.map((result) => Text('Stream result is $result'));
}

void main() {
  testWidgets('StreamBuilderHelperMixin ', (WidgetTester tester) async {
    final controller = StreamController<int>();
    final app = MaterialApp(
      home: Scaffold(
        body: _StreamTestWidget(
          stream: controller.stream,
        ),
      ),
    );
    await tester.pumpWidget(app);
    expect(find.byType(Container), findsOneWidget);

    controller.add(1);
    await tester.pumpAndSettle();
    expect(find.text('Stream result is 1'), findsOneWidget);

    controller.add(2);
    await tester.pumpAndSettle();
    expect(find.text('Stream result is 2'), findsOneWidget);

    await controller.close();
  });

  testWidgets('StreamBuilderHelperMixin with custom defaultWidget',
      (WidgetTester tester) async {
    final controller = StreamController<int>();
    final app = MaterialApp(
      home: Scaffold(
        body: _StreamTestWidgetWithCustomDefaultWidget(
          stream: controller.stream,
        ),
      ),
    );
    await tester.pumpWidget(app);
    expect(find.text('Stream doesn\'t completed yet'), findsOneWidget);

    controller.add(1);
    await tester.pumpAndSettle();
    expect(find.text('Stream result is 1'), findsOneWidget);

    controller.add(2);
    await tester.pumpAndSettle();
    expect(find.text('Stream result is 2'), findsOneWidget);

    await controller.close();
  });
}
