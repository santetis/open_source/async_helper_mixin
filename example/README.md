
##  Example with a Stream

```dart
class MyStreamWidget extends StatelessWidget with StreamHelperMixin {
  final Stream<int> periodicStream;

  MyStreamWidget(this.periodicStream);

  @override
  Stream<Widget> buildStream(BuildContext context) {
    return periodicStream.map((t) => Text('$t elapsed'));
  }

  @override
  Widget get defaultWidget => Text('Waiting stream');
}
```

##  Example with a Future

```dart
class MyFutureWidget extends StatelessWidget with FutureHelperMixin {
  final Future<int> future;

  MyFutureWidget({@required this.future});

  @override
  Future<Widget> buildFuture(BuildContext context) async {
    final value = await future;
    return Text('Future result is $value');
  }

  @override
  Widget get defaultWidget => Text('Waiting future result');
}
```